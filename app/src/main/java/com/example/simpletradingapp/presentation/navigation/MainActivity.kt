package com.example.simpletradingapp.presentation.navigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.simpletradingapp.R
import com.example.simpletradingapp.data.InstrumentsRepo
import com.example.simpletradingapp.data.UserDataRepo
import com.example.simpletradingapp.model.useCases.TradingInstruments
import com.example.simpletradingapp.model.useCases.UserManagement

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.blank)
    }
}

package com.example.simpletradingapp.presentation.favorites.edit

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.Navigation.findNavController
import com.example.simpletradingapp.App
import com.example.simpletradingapp.R
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

class AddFavoritesViewModel(app: Application) : AndroidViewModel(app) {

    private var disposables: CompositeDisposable = CompositeDisposable()

    private val userData = super.getApplication<App>().userData

    private val instrumentsData = super.getApplication<App>().instruments

    var adapter: InstrumentsAddingAdapter = InstrumentsAddingAdapter(
        instrumentsData
            .getAvailableInstruments(),
        userData
            .getLoggedInUser()
            .favoriteInstruments
    )
        private set

    fun doneClicked(view: View) {
        disposables += userData
            .updateUser(
                userData
                    .getLoggedInUser()
                    .copy(
                        favoriteInstruments = adapter.selectedInstruments
                    )
            )
            .subscribe(
                { findNavController(view).navigate(R.id.favoritesEdited) },
                { it.printStackTrace() } // TODO do this better
            )
    }

    fun fragmentStarted() {
        adapter = InstrumentsAddingAdapter(
            instrumentsData
                .getAvailableInstruments(),
            userData
                .getLoggedInUser()
                .favoriteInstruments
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
package com.example.simpletradingapp.presentation.login

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.simpletradingapp.App
import com.example.simpletradingapp.R
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_login_screen.*

class LoginViewModel(app: Application) : AndroidViewModel(app) {

    private var disposables: CompositeDisposable = CompositeDisposable()

    private val userData = super.getApplication<App>().userData

    var autoLoginActive: Boolean = userData
        .isAutoLogin()

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    fun loginClicked(view: View, email: String, password: String, rememberMe: Boolean) {
        disposables += userData
            .login(
                email = email,
                password = password,
                rememberMe = rememberMe
            )
            .subscribe(
                { findNavController(view).navigate(R.id.login_success) },
                { throwable -> throwable.printStackTrace() } // TODO maybe a better error handling
            )
    }

    fun fragmentStarted() {
        autoLoginActive = userData
            .isAutoLogin()

        disposables.clear()

        if (!autoLoginActive) {
            disposables += userData
                .logout()
                .subscribe(
                    {
                        // cleared all data from previous user (if any)
                    },
                    { throwable -> throwable.printStackTrace() } // TODO do this better
                )
        }
    }
}
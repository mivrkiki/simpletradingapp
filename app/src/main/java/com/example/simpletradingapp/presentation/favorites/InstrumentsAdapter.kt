package com.example.simpletradingapp.presentation.favorites

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.simpletradingapp.R
import com.example.simpletradingapp.model.entities.TradingInstrument
import com.example.simpletradingapp.presentation.navigation.MainActivity
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.instrument_with_price.view.*

class InstrumentsAdapter(
    var instruments: List<TradingInstrument>,
    private val onChangeOfCollection: (List<TradingInstrument>) -> Completable
) : RecyclerView.Adapter<InstrumentView>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstrumentView {
        return InstrumentView(
            LayoutInflater.from(parent.context).inflate(
                R.layout.instrument_with_price,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = instruments.size

    override fun onBindViewHolder(holder: InstrumentView, position: Int) {
        holder.name.text = instruments[position].name
        holder.disposables += instruments[position]
            .currentPrice
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { newPrice ->
                    if (newPrice.changeSinceLast > 0) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.price.setBackgroundColor(
                                holder.itemView.context.getColor(R.color.green)
                            )
                            holder.change.setTextColor(holder.itemView.context.getColor(R.color.green))
                            holder.change.text =
                                holder.itemView.context.getString(R.string.positive_move_percentage)
                                    .format(newPrice.percentChangeSinceLast.toString().substring(0, 5))
                        }
                    }
                    if (newPrice.changeSinceLast < 0) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            holder.price.setBackgroundColor(
                                holder.itemView.context.getColor(R.color.red)
                            )
                            holder.change.setTextColor(holder.itemView.context.getColor(R.color.red))
                            holder.change.text =
                                holder.itemView.context.getString(R.string.negative_move_percentage)
                                    .format(newPrice.percentChangeSinceLast.toString().substring(0, 6))
                        }
                    }

                    holder.price.text = newPrice.currentValue.toString()
                },
                { it.printStackTrace() } // TODO something better for error handling
            )

        holder.deleteButton.setOnClickListener {
            holder.disposables += onChangeOfCollection(instruments - instruments[position])
                .subscribe(
                    {
                        instruments = instruments - instruments[position]
                        notifyDataSetChanged()
                    },
                    { it.printStackTrace() } // TODO do this better
                )
        }
    }

    override fun onViewRecycled(holder: InstrumentView) {
        super.onViewRecycled(holder)
        holder.disposables.clear()
    }
}

class InstrumentView(
    view: View
) : RecyclerView.ViewHolder(view) {
    val name = view.instrument_name
    var price = view.instrument_price
    val change = view.percentage_label
    val deleteButton = view.delete_button
    val disposables: CompositeDisposable = CompositeDisposable()
}
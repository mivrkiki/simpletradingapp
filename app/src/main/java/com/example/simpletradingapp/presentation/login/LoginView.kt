package com.example.simpletradingapp.presentation.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.simpletradingapp.R
import kotlinx.android.synthetic.main.fragment_login_screen.*

class LoginView : Fragment() {

    private lateinit var model: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(LoginViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        model.fragmentStarted()
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_screen, container, false)
    }

    override fun onStart() {
        super.onStart()

        loginButton.setOnClickListener { view ->
            model.loginClicked(
                view = view,
                email = emailInput.text.toString(),
                password = passwordInput.text.toString(),
                rememberMe = stayLoggedIn.isChecked
            )
        }

        if (model.autoLoginActive) {
            findNavController().navigate(R.id.login_success)
        }
    }
}

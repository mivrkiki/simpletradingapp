package com.example.simpletradingapp.presentation.favorites.edit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.example.simpletradingapp.R
import com.example.simpletradingapp.model.entities.TradingInstrument
import kotlinx.android.synthetic.main.instrument_selection.view.*

class InstrumentsAddingAdapter(
    private val instruments: List<TradingInstrument>,
    var selectedInstruments: List<TradingInstrument>
) : RecyclerView.Adapter<InstrumentAddingView>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstrumentAddingView {
        return InstrumentAddingView(
            LayoutInflater.from(parent.context).inflate(
                R.layout.instrument_selection,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = instruments.size

    override fun onBindViewHolder(holder: InstrumentAddingView, position: Int) {
        holder.name.text = instruments[position].name
        holder.selected.isChecked = selectedInstruments.contains(instruments[position])

        holder.selected.setOnClickListener { instrument ->
            selectedInstruments = if((instrument as CheckBox).isChecked) {
                selectedInstruments + instruments[position]
            } else {
                selectedInstruments - instruments[position]
            }
        }
    }
}

class InstrumentAddingView(
    view: View
): RecyclerView.ViewHolder(view) {
    val name = view.instrument_name
    val selected = view.selected
}

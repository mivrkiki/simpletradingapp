package com.example.simpletradingapp.presentation.favorites.edit

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.simpletradingapp.R
import com.example.simpletradingapp.presentation.navigation.MainActivity
import kotlinx.android.synthetic.main.fragment_edit_favorites_screen.*

class AddFavoritesView : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_favorites_screen, container, false)
    }

    private lateinit var model: AddFavoritesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(AddFavoritesViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onStart() {
        super.onStart()

        model.fragmentStarted()

        instruments_adding_list.layoutManager = LinearLayoutManager(this.context)

        instruments_adding_list.adapter = model.adapter

        done_button.setOnClickListener { model.doneClicked(it) }
    }
}

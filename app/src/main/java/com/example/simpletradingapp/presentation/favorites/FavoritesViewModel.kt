package com.example.simpletradingapp.presentation.favorites

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.simpletradingapp.App
import com.example.simpletradingapp.R
import com.example.simpletradingapp.model.entities.User
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

class FavoritesViewModel(app: Application) : AndroidViewModel(app) {
    private var disposables: CompositeDisposable = CompositeDisposable()

    private val userData = super.getApplication<App>().userData

    var adapter = InstrumentsAdapter(
        userData
            .getLoggedInUser()
            .favoriteInstruments
    ) {
        userData
            .updateUser(
                User(
                    favoriteInstruments = it
                )
            )
    }

    fun logout(view: View) {
        disposables += userData
            .logout()
            .subscribe(
                { findNavController(view).navigate(R.id.logout) },
                { it.printStackTrace() } // TODO better handling for exceptions
            )
    }

    fun plusClicked(view: View) {
        findNavController(view).navigate(R.id.editFavorites)
    }

    fun fragmentStarted() {
        if (
            userData
                .getLoggedInUser()
                .favoriteInstruments
                .map { it.name }!=
            adapter
                .instruments
                .map { it.name }
        ) {
            adapter
                .instruments = userData
                .getLoggedInUser()
                .favoriteInstruments
            adapter.notifyDataSetChanged()
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
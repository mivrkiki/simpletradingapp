package com.example.simpletradingapp.presentation.favorites

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.simpletradingapp.R
import kotlinx.android.synthetic.main.fragment_favorites_screen.*

class FavoritesView : Fragment() {

    private lateinit var model: FavoritesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(FavoritesViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        model.fragmentStarted()
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites_screen, container, false)
    }


    override fun onStart() {
        super.onStart()

        logout_button.setOnClickListener { model.logout(it) }

        favorites_plus_button.setOnClickListener { model.plusClicked(it) }

        instruments_list.layoutManager = LinearLayoutManager(this.context)

        instruments_list.adapter = model.adapter
    }
}

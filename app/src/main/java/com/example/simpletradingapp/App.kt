package com.example.simpletradingapp

import android.app.Application
import android.content.Context
import com.example.simpletradingapp.data.InstrumentsRepo
import com.example.simpletradingapp.data.UserDataRepo
import com.example.simpletradingapp.model.useCases.TradingInstruments
import com.example.simpletradingapp.model.useCases.UserManagement

class App: Application() {

    lateinit var userData: UserManagement
    val instruments: TradingInstruments = InstrumentsRepo()

    override fun onCreate() {
        super.onCreate()

        userData = UserDataRepo(getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE))
    }

    companion object {
        const val STORAGE_NAME = "SimpleTrading"
    }
}
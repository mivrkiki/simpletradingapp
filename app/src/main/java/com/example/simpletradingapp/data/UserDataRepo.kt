package com.example.simpletradingapp.data

import android.content.SharedPreferences
import com.example.simpletradingapp.model.entities.User
import com.example.simpletradingapp.model.useCases.UserManagement
import io.reactivex.Completable
import java.io.IOException

class UserDataRepo(
    private val sharedPreferences: SharedPreferences
) : UserManagement {

    private var favorites: List<String>
    private var rememberMe: Boolean = isAutoLogin()

    init {
        favorites = sharedPreferences
            .getString(STORAGE_KEY, "")
            ?.split(" ")!!
    }

    override fun login(email: String, password: String, rememberMe: Boolean): Completable {
        this.rememberMe = rememberMe
        return if (rememberMe) {
            if (sharedPreferences
                    .edit()
                    .clear()
                    .putString(STORAGE_KEY, "")
                    .commit()
            ) {
                Completable.complete()
            } else {
                Completable.error {
                    IOException("Failed to write to shared storage")
                }
            }
        } else {
            Completable.complete()
        }
    }

    override fun isAutoLogin(): Boolean = sharedPreferences
        .contains(STORAGE_KEY)

    override fun getLoggedInUser(): User =
        User(
            favoriteInstruments = FakeBE.instruments
                .filter {
                    favorites.contains(it.name)
                }
        )

    override fun updateUser(user: User): Completable {
        return if (rememberMe) {
            if (sharedPreferences
                    .edit()
                    .putString(STORAGE_KEY, user.favoriteInstruments.joinToString(" ") { it.name })
                    .commit()
            ) {
                favorites = user.favoriteInstruments.map { it.name }
                Completable.complete()
            } else {
                Completable.error {
                    IOException("Failed to write to shared storage")
                }
            }
        } else {
            favorites = user.favoriteInstruments.map { it.name }
            Completable.complete()
        }
    }

    override fun logout(): Completable {
        return if (sharedPreferences
                .edit()
                .clear()
                .commit()
        ) {
            favorites = listOf()
            Completable.complete()
        } else {
            Completable.error {
                IOException("Failed to write to shared storage")
            }
        }
    }

    companion object {
        const val STORAGE_KEY = "Favorites"
    }
}
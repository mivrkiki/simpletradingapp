package com.example.simpletradingapp.data

import com.example.simpletradingapp.model.useCases.TradingInstruments

class InstrumentsRepo: TradingInstruments {
    override fun getAvailableInstruments() = FakeBE.instruments
}
package com.example.simpletradingapp.data

import com.example.simpletradingapp.model.entities.PriceWithChange
import com.example.simpletradingapp.model.entities.TradingInstrument
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import kotlin.random.Random

object FakeBE {
    private const val INITIAL_DELAY: Long = 0
    private const val INTERVAL: Long = 1000

    data class DummyData(val name: String, val minValue: Double, val maxValue: Double)

    private val dummyInstruments = listOf<DummyData>(
        DummyData("Gold", 1135.32, 1142.95),
        DummyData("MasterCard", 102.1, 105.8),
        DummyData("EUR/USD", 1.04321, 1.04365),
        DummyData("UK100", 7014.8, 7019.6),
        DummyData("BMW", 88.5, 92.3),
        DummyData("US30", 19899.1, 19905.0),
        DummyData("Barclays", 2.2181, 2.2248),
        DummyData("Bitcoin", 776.03, 783.93),
        DummyData("VW", 130.10, 134.80),
        DummyData("SWISS20", 8228.3, 8237.8),
        DummyData("Alcoa", 27.365, 30.584),
        DummyData("NDAQ100", 4956.4, 4961.6),
        DummyData("ITALIAN40", 18949.0, 18968.0),
        DummyData("AberdeenAM", 2.5350, 2.5620),
        DummyData("Apple", 116.15, 120.64)
    )

    private val timer = Observable.interval(INITIAL_DELAY, INTERVAL, TimeUnit.MILLISECONDS)
        .publish()
        .refCount()

    val instruments =
        dummyInstruments
            .map { dummyData ->
                TradingInstrument(
                    name = dummyData.name,
                    currentPrice = timer
                        .map {
                            Random.nextDouble(dummyData.minValue, dummyData.maxValue).toFloat()
                        }
                        .subscribeOn(Schedulers.computation())
                        .toFlowable(BackpressureStrategy.ERROR)
                        .map {
                            PriceWithChange(
                                currentValue = it,
                                changeSinceLast = Float.NaN,
                                percentChangeSinceLast = Float.NaN
                            )
                        }
                        .scan { oldPriceWithChange: PriceWithChange, newPrice: PriceWithChange ->
                            PriceWithChange(
                                currentValue = newPrice.currentValue,
                                changeSinceLast = newPrice.currentValue - oldPriceWithChange.currentValue,
                                percentChangeSinceLast = (newPrice.currentValue - oldPriceWithChange.currentValue) * 100 / newPrice.currentValue
                            )
                        }
                        .replay(1)
                        .refCount()
                )
            }
}
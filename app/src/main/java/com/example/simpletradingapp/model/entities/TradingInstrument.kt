package com.example.simpletradingapp.model.entities

import io.reactivex.Flowable

data class TradingInstrument(
    val name: String,
    val currentPrice: Flowable<PriceWithChange>
)

data class PriceWithChange(
    val currentValue: Float,
    val changeSinceLast: Float,
    val percentChangeSinceLast: Float
)

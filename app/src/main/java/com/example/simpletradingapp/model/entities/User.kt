package com.example.simpletradingapp.model.entities

data class User (
    var favoriteInstruments: List<TradingInstrument>
)
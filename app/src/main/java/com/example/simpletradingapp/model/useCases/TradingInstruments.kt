package com.example.simpletradingapp.model.useCases

import com.example.simpletradingapp.model.entities.TradingInstrument

interface TradingInstruments {

    // returns a list of all available instruments
    fun getAvailableInstruments(): List<TradingInstrument>
}
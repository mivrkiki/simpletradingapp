package com.example.simpletradingapp.model.useCases

import com.example.simpletradingapp.model.entities.User
import io.reactivex.Completable

interface UserManagement {
    // Logs in a user if it's available in the memory, creates a new user if no such user is available
    fun login(email: String, password: String, rememberMe: Boolean): Completable

    // returns true if the auto login is enabled and there is a user in the storage of the app
    fun isAutoLogin(): Boolean

    // Returns the logged in user if available or throws exception when no user is logged in
    fun getLoggedInUser(): User

    // updates the user's favorites
    fun updateUser(user: User): Completable

    // logs out the user
    fun logout(): Completable
}